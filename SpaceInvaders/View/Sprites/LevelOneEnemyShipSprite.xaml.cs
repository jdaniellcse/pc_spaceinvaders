﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SpaceInvaders.View.Sprites
{
    /// <summary>
    ///     Draws a level one enemy ship.
    ///     Must implemente the ISpriteRenderer so will be displayed as a game object.
    /// </summary>
    /// <seealso cref="Windows.UI.Xaml.Controls.UserControl" />
    /// <seealso cref="SpaceInvaders.View.Sprites.ISpriteRenderer" />
    public sealed partial class LevelOneEnemyShipSprite : ISpriteRenderer
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="LevelOneEnemyShipSprite" /> class.
        ///     Precondition: none
        ///     Postconditon: Sprite created.
        /// </summary>
        public LevelOneEnemyShipSprite()
        {
            this.InitializeComponent();
        }
        /// <summary>
        /// Animates the ship.
        /// </summary>
        public void AnimateShip()
        {
            if (this.animatedFin1.Visibility == Visibility.Collapsed &&
                this.animatedFin2.Visibility == Visibility.Collapsed)
            {
                this.animatedFin1.Visibility = Visibility.Visible;
                this.animatedFin2.Visibility = Visibility.Visible;
            }
            else
            {
                this.animatedFin1.Visibility = Visibility.Collapsed;
                this.animatedFin2.Visibility = Visibility.Collapsed;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Renders the sprite at the specified location.
        ///     Precondition: none
        ///     Postcondition: sprite drawn at location (x,y)
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        public void RenderAt(double x, double y)
        {
            Canvas.SetLeft(this, x);
            Canvas.SetTop(this, y);
        }

        #endregion
    }
}