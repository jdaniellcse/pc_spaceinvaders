﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Manages a LivesBox.
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.GameObject" />
    public class LivesBox : GameObject
    {
        #region Data members

        private readonly LivesBoxSprite livesBoxSprite;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ScoreBox" /> class.
        /// </summary>
        public LivesBox()
        {
            this.livesBoxSprite = new LivesBoxSprite();
            Sprite = this.livesBoxSprite;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Sets the text for the scoreBox
        /// </summary>
        /// <param name="value">The text in the scoreBox</param>
        public void SetText(string value)
        {
            this.livesBoxSprite.SetText(value);
        }

        #endregion
    }
}