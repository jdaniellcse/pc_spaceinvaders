﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Manages an enemy ship.
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.GameObject" />
    public class EnemyShip : GameObject
    {
        #region Data members

        private const int SpeedXDirection = 5;
        private const int SpeedYDirection = 0;
        private readonly LevelOneEnemyShipSprite levelOneEnemySprite;
        private readonly LevelTwoEnemyShipSprite levelTwoEnemySprite;
        private readonly LevelThreeEnemyShipSprite levelThreeEnemySprite;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShip" /> class.
        /// </summary>
        public EnemyShip()
        {
            SetSpeed(SpeedXDirection, SpeedYDirection);
            this.levelOneEnemySprite = new LevelOneEnemyShipSprite();
            this.levelTwoEnemySprite = new LevelTwoEnemyShipSprite();
            this.levelThreeEnemySprite = new LevelThreeEnemyShipSprite();
        }

        /// <summary>
        /// Animates the enemy ship.
        /// </summary>
        public void AnimateEnemyShip()
        {
            this.levelOneEnemySprite.AnimateShip();
            this.levelTwoEnemySprite.AnimateShip();
            this.levelThreeEnemySprite.AnimateShip();
        }

        //// <summary>
        /// Initializes the level one ship.
        ///// </summary>
        public void InitializeLevelOneShip()
        {
            Sprite = this.levelOneEnemySprite;
        }
        /// <summary>
        /// Initializes the level two ship.
        /// </summary>
        public void InitializeLevelTwoShip()
        {
            Sprite = this.levelTwoEnemySprite;
        }
        /// <summary>
        /// Initializes the level three ship.
        /// </summary>
        public void InitializeLevelThreeShip()
        {
            Sprite = this.levelThreeEnemySprite;
        }
        #endregion
    }
}
