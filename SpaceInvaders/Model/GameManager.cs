﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Manages the entire game.
    /// </summary>
    public class GameManager

    {
        #region Data members

        /// <summary>
        ///     The tick interval for ships
        /// </summary>
        public const int ShipInterval = 200;

        /// <summary>
        ///     The tick interval for bullets
        /// </summary>
        public const int BulletInterval = 20;

        /// <summary>
        ///     High response time interval for game score and end state conditions
        /// </summary>
        public const int StateInterval = 1;

        private const double PlayerShipBottomOffset = 30;
        private const double ShipWidth = 50;
        private const double ShipHeight = 33;
        private const double EnemyShipOffset = 60;
        private const double BulletDelaySpacing = 20;
        private const int NumberOfTicksForInitialMovement = 10;
        private const int InitialMovementPlusFullMovementAcrossScreen = 30;
        private const int InitialMovementPlusMovementAcrossScreenTwice = 50;
        private const int NumberOfShipsInFirstRow = 2;
        private const int NumberOfShipsInSecondRow = 4;
        private const int NumberOfShipsInThirdRow = 6;
        private const int NumberOfShipsInFourthRow = 8;

        private readonly TimeSpan gameTickInterval = new TimeSpan(0, 0, 0, 0, StateInterval);
        private readonly TimeSpan shipTickInterval = new TimeSpan(0, 0, 0, 0, ShipInterval);
        private readonly TimeSpan bulletTickInterval = new TimeSpan(0, 0, 0, 0, BulletInterval);
        private readonly double backgroundHeight;
        private readonly double backgroundWidth;
        private readonly List<EnemyShip> levelOneEnemyList;
        private readonly List<EnemyShip> levelTwoEnemyList;
        private readonly List<EnemyShip> levelThreeEnemyList;
        private readonly List<EnemyShip> levelFourEnemyList;
        private readonly List<List<EnemyShip>> enemyFleet;
        private readonly DispatcherTimer enemyFleetTimer;
        private readonly DispatcherTimer bulletTimer;
        private readonly DispatcherTimer enemyBulletTimer;
        private readonly DispatcherTimer collisionTimer;
        private int enemyFleetTickCounter;
        private bool fleetHasMovedOneHalfScreen;
        private Random bulletChanceRow3;
        private Random bulletChanceRow4;
        private readonly Random shipToFire;
        private double playerScore;
        private readonly List<Bullet> playerBulletList;
        private readonly List<Bullet> enemyBulletList;
        private readonly ScoreBox scoreBox;
        private readonly LivesBox livesBox;

        private Bullet playerBullet;
        private Canvas theCanvas;

        private PlayerShip playerShip;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="GameManager" /> class.
        ///     Precondition: backgroundHeight > 0 AND backgroundWidth > 0
        /// </summary>
        /// <param name="backgroundHeight">The backgroundHeight of the game play window.</param>
        /// <param name="backgroundWidth">The backgroundWidth of the game play window.</param>
        public GameManager(double backgroundHeight, double backgroundWidth)
        {
            if (backgroundHeight <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(backgroundHeight));
            }

            if (backgroundWidth <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(backgroundWidth));
            }

            this.levelOneEnemyList = new List<EnemyShip>();
            this.levelTwoEnemyList = new List<EnemyShip>();
            this.levelThreeEnemyList = new List<EnemyShip>();
            this.levelFourEnemyList = new List<EnemyShip>();
            this.enemyFleet = new List<List<EnemyShip>>();
            this.playerBulletList = new List<Bullet>();
            this.enemyBulletList = new List<Bullet>();

            this.scoreBox = new ScoreBox();
            this.livesBox = new LivesBox();

            this.playerScore = 0;

            this.shipToFire = new Random();

            this.backgroundHeight = backgroundHeight;
            this.backgroundWidth = backgroundWidth;
            this.enemyFleetTickCounter = 0;
            this.fleetHasMovedOneHalfScreen = false;

            this.enemyFleetTimer = new DispatcherTimer {Interval = this.shipTickInterval};
            this.enemyFleetTimer.Tick += this.enemyFleetTimerOnTick;
            this.enemyFleetTimer.Start();

            this.bulletTimer = new DispatcherTimer {Interval = this.bulletTickInterval};
            this.bulletTimer.Tick += this.bulletTimerOnTick;
            this.bulletTimer.Start();

            this.enemyBulletTimer = new DispatcherTimer {Interval = this.bulletTickInterval};
            this.enemyBulletTimer.Tick += this.enemyBulletTimerOnTick;
            this.enemyBulletTimer.Start();

            this.collisionTimer = new DispatcherTimer {Interval = this.bulletTickInterval};
            this.collisionTimer.Tick += this.collisionTimerOnTick;

            var gameStateTimer = new DispatcherTimer {Interval = this.gameTickInterval};
            gameStateTimer.Tick += this.stateTimerOnTick;
            gameStateTimer.Start();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Initializes the game placing player ship and enemy ship in the game.
        ///     Precondition: background != null
        ///     Postcondition: Game is initialized and ready for play.
        /// </summary>
        /// <param name="background">The background canvas.</param>
        public void InitializeGame(Canvas background)
        {
            if (background == null)
            {
                throw new ArgumentNullException(nameof(background));
            }
            this.theCanvas = background;
            this.createAndPlacePlayerShip();
            this.createAndAddEnemyShips();
            this.initializeBoxes();
        }

        private void initializeBoxes()
        {
            this.theCanvas.Children.Add(this.scoreBox.Sprite);
            this.theCanvas.Children.Add(this.livesBox.Sprite);

            this.livesBox.X = this.backgroundWidth - this.livesBox.Width;
        }

        private void initializeEnemyRows()
        {
            for (var i = 0; i < NumberOfShipsInFirstRow; i++)
            {
                this.levelOneEnemyList.Add(new EnemyShip());
            }
            foreach (var enemy in this.levelOneEnemyList)
            {
                this.createLevelOneShip(enemy);
            }

            for (var i = 0; i < NumberOfShipsInSecondRow; i++)
            {
                this.levelTwoEnemyList.Add(new EnemyShip());
            }
            foreach (var enemy in this.levelTwoEnemyList)
            {
                this.createLevelTwoShip(enemy);
            }

            for (var i = 0; i < NumberOfShipsInThirdRow; i++)
            {
                this.levelThreeEnemyList.Add(new EnemyShip());
            }
            foreach (var enemy in this.levelThreeEnemyList)
            {
                this.createLevelThreeShip(enemy);
            }

            for (var i = 0; i < NumberOfShipsInFourthRow; i++)
            {
                this.levelFourEnemyList.Add(new EnemyShip());
            }
            foreach (var enemy in this.levelFourEnemyList)
            {
                this.createLevelThreeShip(enemy);
            }

            this.enemyFleet.Add(this.levelOneEnemyList);
            this.enemyFleet.Add(this.levelTwoEnemyList);
            this.enemyFleet.Add(this.levelThreeEnemyList);
            this.enemyFleet.Add(this.levelFourEnemyList);
        }

        private void createLevelOneShip(EnemyShip enemy)
        {
            enemy.InitializeLevelOneShip();
        }

        private void createLevelTwoShip(EnemyShip enemy)
        {
            enemy.InitializeLevelTwoShip();
        }

        private void createLevelThreeShip(EnemyShip enemy)
        {
            enemy.InitializeLevelThreeShip();
        }

        private void createAndPlacePlayerShip()
        {
            this.playerShip = new PlayerShip();
            this.theCanvas.Children.Add(this.playerShip.Sprite);

            this.placePlayerShipNearBottomOfBackgroundCentered();
        }

        private void createAndAddEnemyShips()
        {
            this.initializeEnemyRows();

            foreach (var enemy in this.levelOneEnemyList)
            {
                this.theCanvas.Children.Add(enemy.Sprite);
            }

            foreach (var enemy in this.levelTwoEnemyList)
            {
                this.theCanvas.Children.Add(enemy.Sprite);
            }

            foreach (var enemy in this.levelThreeEnemyList)
            {
                this.theCanvas.Children.Add(enemy.Sprite);
            }

            foreach (var enemy in this.levelFourEnemyList)
            {
                this.theCanvas.Children.Add(enemy.Sprite);
            }

            this.centerEnemyShips();
        }

        private void centerEnemyShips()
        {
            this.centerRowOneEnemies();

            this.centerRowTwoEnemies();

            this.centerRowThreeEnemies();

            this.centerRowFourEnemies();
        }

        private void centerRowOneEnemies()
        {
            var initial = this.backgroundWidth/2 - EnemyShipOffset;

            foreach (var enemy in this.levelOneEnemyList)
            {
                enemy.X = initial;
                initial += EnemyShipOffset;
                enemy.Y = this.backgroundHeight/3 + 25;
            }
        }

        private void centerRowTwoEnemies()
        {
            var initial = this.backgroundWidth/2 - EnemyShipOffset*2;

            foreach (var enemy in this.levelTwoEnemyList)
            {
                enemy.X = initial;
                initial += EnemyShipOffset;
                enemy.Y = this.backgroundHeight/3 - 25;
            }
        }

        private void centerRowThreeEnemies()
        {
            var initial = this.backgroundWidth/2 - EnemyShipOffset*3;

            foreach (var enemy in this.levelThreeEnemyList)
            {
                enemy.X = initial;
                initial += EnemyShipOffset;
                enemy.Y = this.backgroundHeight/3 - 75;
            }
        }

        private void centerRowFourEnemies()
        {
            var initial = this.backgroundWidth/2 - EnemyShipOffset*4;

            foreach (var enemy in this.levelFourEnemyList)
            {
                enemy.X = initial;
                initial += EnemyShipOffset;
                enemy.Y = this.backgroundHeight/3 - 125;
            }
        }

        private void placePlayerShipNearBottomOfBackgroundCentered()
        {
            this.playerShip.X = this.backgroundWidth/2 - this.playerShip.Width/2.0;
            this.playerShip.Y = this.backgroundHeight - this.playerShip.Height - PlayerShipBottomOffset;
        }

        private void stateTimerOnTick(object sender, object e)
        {
            this.checkForWinCondition();
        }

        private void collisionTimerOnTick(object sender, object e)
        {
            this.scoreBox.SetText("Player Score: " + this.playerScore);
            foreach (var bullet in this.playerBulletList.ToArray())
            {
                this.checkCollisionForRowOne(bullet);
                this.checkCollisionForRowTwo(bullet);
                this.checkCollisionForRowThree(bullet);
                this.checkCollisionForRowFour(bullet);
            }
        }

        private void checkForWinCondition()
        {
            if (this.enemyFleetIsEmpty())
            {
                this.enemyBulletTimer.Stop();
                this.collisionTimer.Stop();
                this.enemyBulletTimer.Stop();
                this.bulletTimer.Stop();

                this.clearStrayBulletsAfterGameEnds();

                this.scoreBox.SetText("FINAL SCORE: " + this.playerScore + " YOU WIN");
                this.theCanvas.Children.Remove(this.playerShip.Sprite);
            }
        }

        private void clearStrayBulletsAfterGameEnds()
        {
            foreach (var bullet in this.enemyBulletList)
            {
                this.theCanvas.Children.Remove(bullet.Sprite);
            }

            foreach (var bullet in this.playerBulletList)
            {
                this.theCanvas.Children.Remove(bullet.Sprite);
            }
        }

        private bool enemyFleetIsEmpty()
        {
            return this.levelOneEnemyList.Count == 0 && this.levelTwoEnemyList.Count == 0 &&
                   this.levelThreeEnemyList.Count == 0 && this.levelFourEnemyList.Count == 0;
        }

        private void checkCollisionForRowFour(Bullet bullet)
        {
            for (var i = 0; i < this.levelFourEnemyList.Count; i++)
                if (bullet.X >= this.levelFourEnemyList[i].X &&
                    bullet.X <= this.levelFourEnemyList[i].X + ShipWidth &&
                    bullet.Y >= this.levelFourEnemyList[i].Y && 
                    bullet.Y <= this.levelFourEnemyList[i].Y + ShipHeight)
                {
                    this.playerScore += 400;
                    this.theCanvas.Children.Remove(bullet.Sprite);
                    this.theCanvas.Children.Remove(this.levelFourEnemyList[i].Sprite);
                    this.playerBulletList.Remove(bullet);
                    this.levelFourEnemyList.Remove(this.levelFourEnemyList[i]);
                }
        }

        private void checkCollisionForRowThree(Bullet bullet)
        {
            for (var i = 0; i < this.levelThreeEnemyList.Count; i++)
                if (bullet.X >= this.levelThreeEnemyList[i].X && 
                    bullet.X <= this.levelThreeEnemyList[i].X + ShipWidth &&
                    bullet.Y >= this.levelThreeEnemyList[i].Y && 
                    bullet.Y <= this.levelThreeEnemyList[i].Y + ShipHeight)
                {
                    this.playerScore += 300;
                    this.theCanvas.Children.Remove(bullet.Sprite);
                    this.theCanvas.Children.Remove(this.levelThreeEnemyList[i].Sprite);
                    this.playerBulletList.Remove(bullet);
                    this.levelThreeEnemyList.Remove(this.levelThreeEnemyList[i]);
                }
        }

        private void checkCollisionForRowTwo(Bullet bullet)
        {
            for (var i = 0; i < this.levelTwoEnemyList.Count; i++)
                if (bullet.X >= this.levelTwoEnemyList[i].X &&
                    bullet.X <= this.levelTwoEnemyList[i].X + ShipWidth &&
                    bullet.Y >= this.levelTwoEnemyList[i].Y &&
                    bullet.Y <= this.levelTwoEnemyList[i].Y + ShipHeight)
                {
                    this.playerScore += 200;
                    this.theCanvas.Children.Remove(bullet.Sprite);
                    this.theCanvas.Children.Remove(this.levelTwoEnemyList[i].Sprite);
                    this.playerBulletList.Remove(bullet);
                    this.levelTwoEnemyList.Remove(this.levelTwoEnemyList[i]);
                }
        }

        private void checkCollisionForRowOne(Bullet bullet)
        {
            for (var i = 0; i < this.levelOneEnemyList.Count; i++)
                if (bullet.X >= this.levelOneEnemyList[i].X &&
                    bullet.X <= this.levelOneEnemyList[i].X + ShipWidth &&
                    bullet.Y >= this.levelOneEnemyList[i].Y &&
                    bullet.Y <= this.levelOneEnemyList[i].Y + ShipHeight)
                {
                    this.playerScore += 100;
                    this.theCanvas.Children.Remove(bullet.Sprite);
                    this.theCanvas.Children.Remove(this.levelOneEnemyList[i].Sprite);
                    this.playerBulletList.Remove(bullet);
                    this.levelOneEnemyList.Remove(this.levelOneEnemyList[i]);
                }
        }

        private void enemyBulletTimerOnTick(object sender, object e)
        {
            this.tryToFireEnemyBullet();

            foreach (var bullet in this.enemyBulletList.ToArray())
            {
                if (bullet.Y >= this.backgroundHeight)
                {
                    this.enemyBulletList.Remove(bullet);
                    return;
                }
                bullet.Y += bullet.SpeedY;
                this.checkForPlayerCollision(bullet);
            }
        }

        private void tryToFireEnemyBullet()
        {
            this.bulletChanceRow3 = new Random();
            this.bulletChanceRow4 = new Random();

            var chanceRow3 = this.bulletChanceRow3.Next(0, 250);

            if (chanceRow3 >= 0 && chanceRow3 <= 5)
            {
                this.createEnemyBulletRowThree();
            }

            var chanceRow4 = this.bulletChanceRow4.Next(0, 150);

            if (chanceRow4 >= 100 && chanceRow4 <= 105)
            {
                this.createEnemyBulletRowFour();
            }
        }

        /// <summary>
        ///     Checks for player collision.
        /// </summary>
        /// <param name="bullet">The bullet.</param>
        private void checkForPlayerCollision(Bullet bullet)
        {
            if (bullet.X >= this.playerShip.X && bullet.X <= this.playerShip.X + ShipWidth &&
                bullet.Y >= this.playerShip.Y && bullet.Y <= this.playerShip.Y + ShipHeight)
            {
                if (this.playerShip.Lives > 1)
                {
                    this.enemyBulletList.Remove(bullet);
                    this.theCanvas.Children.Remove(bullet.Sprite);
                }
                else
                {
                    this.theCanvas.Children.Remove(bullet.Sprite);
                    this.theCanvas.Children.Remove(this.playerShip.Sprite);
                    this.scoreBox.SetText("GAME OVER");
                    this.enemyBulletTimer.Stop();
                    this.enemyFleetTimer.Stop();
                    this.bulletTimer.Stop();
                    this.collisionTimer.Stop();
                    this.enemyFleetTimer.Stop();

                    this.clearStrayBulletsAfterGameEnds();
                }

                this.playerShip.Lives--;
                this.livesBox.SetText("Player Lives: " + this.playerShip.Lives);
            }
        }

        private void bulletTimerOnTick(object sender, object e)
        {
            foreach (var bullet in this.playerBulletList)
            {
                if (this.bulletIsOffScreen(bullet))
                {
                    return;
                }

                bullet.Y -= bullet.SpeedY;
            }
        }

        private bool bulletIsOffScreen(Bullet bullet)
        {
            if (bullet.Y < -bullet.Height)
            {
                this.playerBulletList.Remove(bullet);
                return true;
            }
            return false;
        }

        private void enemyFleetTimerOnTick(object sender, object o)
        {
            foreach (var row in this.enemyFleet)
            {
                foreach (var enemy in row)
                {
                    enemy.AnimateEnemyShip();
                }
            }
            if (this.enemyFleetTickCounter < NumberOfTicksForInitialMovement && !this.fleetHasMovedOneHalfScreen)
            {
                this.moveEnemyFleetRight();
                this.enemyFleetTickCounter++;
            }

            else if (this.enemyFleetTickCounter < InitialMovementPlusFullMovementAcrossScreen)
            {
                this.moveEnemyFleetLeft();
                this.enemyFleetTickCounter++;
                this.fleetHasMovedOneHalfScreen = true;
            }
            else if (this.enemyFleetTickCounter >= InitialMovementPlusFullMovementAcrossScreen &&
                     this.enemyFleetTickCounter < InitialMovementPlusMovementAcrossScreenTwice)
            {
                this.moveEnemyFleetRight();
                this.enemyFleetTickCounter++;
            }
            else
            {
                this.enemyFleetTickCounter = NumberOfTicksForInitialMovement;
            }
        }

        private void moveEnemyFleetLeft()
        {
            foreach (var row in this.enemyFleet)
            {
                foreach (var enemy in row)
                {
                    enemy.X -= enemy.SpeedX;
                }
            }
        }

        private void moveEnemyFleetRight()
        {
            foreach (var row in this.enemyFleet)
            {
                foreach (var enemy in row)
                {
                    enemy.X += enemy.SpeedX;
                }
            }
        }

        /// <summary>
        ///     Moves the player ship to the left.
        ///     Precondition: none
        ///     Postcondition: The player ship has moved left.
        /// </summary>
        public void MovePlayerShipLeft()
        {
            this.playerShip.MoveLeft();
        }

        /// <summary>
        ///     Moves the player ship to the right.
        ///     Precondition: none
        ///     Postcondition: The player ship has moved right.
        /// </summary>
        public void MovePlayerShipRight()
        {
            this.playerShip.MoveRight();
        }

        /// <summary>
        ///     Creates the player bullet.
        /// </summary>
        public void CreatePlayerBullet()
        {
            if (this.playerBulletList.Count >= 3)
            {
                return;
            }

            else if (this.playerBulletList.Count == 0)
            {
                this.playerBullet = new Bullet {
                    X = this.playerShip.X + this.playerShip.Width/2,
                    Y = this.playerShip.Y
                };
                this.playerBulletList.Add(this.playerBullet);
                this.theCanvas.Children.Add(this.playerBullet.Sprite);
            }
            else
            {
                this.playerBullet = new Bullet {
                    X = this.playerShip.X + this.playerShip.Width/2,
                    Y = this.playerShip.Y
                };

                foreach (var bullet in this.playerBulletList)
                {
                    if (this.playerBullet.Y >= bullet.Y &&
                        this.playerBullet.Y <= bullet.Y + this.playerBullet.Height + BulletDelaySpacing)
                    {
                        return;
                    }
                }

                this.playerBulletList.Add(this.playerBullet);
                this.theCanvas.Children.Add(this.playerBullet.Sprite);
            }

            this.collisionTimer.Start();
        }

        private void createEnemyBulletRowThree()
        {
            if (this.levelThreeEnemyList.Count != 0)
            {
                var enemyBullet = new Bullet();
                this.enemyBulletList.Add(enemyBullet);
                this.theCanvas.Children.Add(enemyBullet.Sprite);

                enemyBullet.X = this.levelThreeEnemyList[this.shipToFire.Next(0, this.levelThreeEnemyList.Count)].X +
                                ShipWidth/2;
                enemyBullet.Y = this.levelThreeEnemyList[0].Y;
            }
        }

        private void createEnemyBulletRowFour()
        {
            if (this.levelFourEnemyList.Count != 0)
            {
                var enemyBullet = new Bullet();
                this.enemyBulletList.Add(enemyBullet);
                this.theCanvas.Children.Add(enemyBullet.Sprite);

                enemyBullet.X = this.levelFourEnemyList[this.shipToFire.Next(0, this.levelFourEnemyList.Count)].X +
                                ShipWidth/2;
                enemyBullet.Y = this.levelFourEnemyList[0].Y;
            }
        }

        #endregion
    }
}