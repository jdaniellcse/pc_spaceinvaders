﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Manages an ScoreBox.
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.GameObject" />
    public class ScoreBox : GameObject
    {
        #region Data members

        private readonly ScoreBoxSprite scoreBoxSprite;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ScoreBox" /> class.
        /// </summary>
        public ScoreBox()
        {
            this.scoreBoxSprite = new ScoreBoxSprite();
            Sprite = this.scoreBoxSprite;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Sets the text for the scoreBox
        /// </summary>
        /// <param name="value">The text in the scoreBox</param>
        public void SetText(string value)
        {
            this.scoreBoxSprite.SetText(value);
        }

        #endregion
    }
}